ACKNOWLEDGEMENTS
================

A lot of people deserve thanks for improving CL-UNIFICATION.

The Lisp NYC group has endured presentations of this code and provided
feedback.

The following individuals have provided feedback and (precious) bug
fixes.

Boldyrev, Ivan
Brown, Robert
Korablin, Vladimir V.
Leuner, John
McManus, Russell
Scott, Peter
Takagi, Masayuki
Werner, Norman
