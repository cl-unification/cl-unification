CL-UNIFICATION
==============

Marco Antoniotti (c) 2004-2025
------------------------------

The directory containing this file you are reading should contain the
code and the documentation of the CL-UNIFICATION package.

The package is a full-blown library to "unify" arbitrary CL objects
while constructing bindings for placeholders (unification variables)
in a "template" sublanguage.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.

Enjoy.
