;;;; -*- Mode: Lisp -*-

;;;; unification-package.lisp --
;;;; Package definition for the CL-UNIFICATION library.
;;;;
;;;; See file COPYING for copyright and licensing information.


(defpackage "IT.UNIMIB.DISCO.MA.CL.EXT.DACF.UNIFICATION" (:use "CL")
  (:nicknames  "CL.EXT.DACF.UNIFICATION" "UNIFY" "unify" "CL-UNIFICATION")
  (:documentation "The CL.EXT.DACF.UNIFICATION Package.

This package contains all the definitions necessary for the general
Common Lisp unifier to work.
The package also has the \"UNIFY\" nickname.")

  (:export
   "*UNIFY-STRING-CASE-SENSITIVE-P*"
   "UNIFY"

   "APPLY-SUBSTITUTION"

   "UNIFICATION-FAILURE"
   "UNIFICATION-VARIABLE-UNBOUND"
   )

  (:export
   "ENVIRONMENT"
   "ENVIRONMENT-P"
   "MAKE-EMPTY-ENVIRONMENT"
   "EMPTY-ENVIRONMENT-P"
   "MAKE-SHARED-ENVIRONMENT"
   "COPY-ENVIRONMENT"

   "SUBSTITUTION"
   
   "PUSH-FRAME"
   "POP-FRAME"

   "BINDING-VARIABLE"
   "BINDING-VALUE"

   "EXTEND-ENVIRONMENT"
   "FILL-ENVIRONMENT"
   "FILL-ENVIRONMENT*"

   "FIND-VARIABLE-VALUE"
   "V?"

   "NEW-VAR"
   "VARIABLEP"
   "VARIABLE-ANY-P"
   )

  (:export
   "MATCH"
   "MATCHF"
   "MATCHING"
   "MATCH-CASE"
   "MATCHF-CASE"
   )

  (:export
   "UNIFY*"
   "UNIFY-EQUATIONS"
   "UNIFY-EQUATIONS*")

  (:export
   "MAKE-TEMPLATE"
   "TEMPLATE"
   "TEMPLATE-P"
   "TEMPLATE-SPEC"

   "COLLECT-TEMPLATE-VARS"
   )

  (:export
   "EXPRESSION-TEMPLATE"
   "ELEMENT-TEMPLATE"
   "AREF-TEMPLATE"
   "ELT-TEMPLATE"
   "NTH-TEMPLATE"
   "NTHCDR-TEMPLATE"
   "TYPE-TEMPLATE"
   "ARRAY-TEMPLATE"
   "NIL-TEMPLATE"
   "NUMBER-TEMPLATE"
   "SYMBOL-TEMPLATE"
   "STANDARD-OBJECT-TEMPLATE"
   "STRUCTURE-OBJECT-TEMPLATE"

   "EXPRESSION-TEMPLATE-P"
   "ELEMENT-TEMPLATE-P"
   "AREF-TEMPLATE-P"
   "ELT-TEMPLATE-P"
   "NTH-TEMPLATE-P"
   "NTHCDR-TEMPLATE-P"
   "TYPE-TEMPLATE-P"
   "ARRAY-TEMPLATE-P"
   "NIL-TEMPLATE-P"
   "NUMBER-TEMPLATE-P"
   "SYMBOL-TEMPLATE-P"
   "STANDARD-OBJECT-TEMPLATE-P"
   "STRUCTURE-OBJECT-TEMPLATE-P"
   )

  #+cl-ppcre
  (:export
   "REGULAR-EXPRESSION-TEMPLATE"
   "REGULAR-EXPRESSION-TEMPLATE-P"
   "REGULAR-EXPRESSION"
   "REGEXP")
  )

;;;; end of file -- unification-package.lisp --
