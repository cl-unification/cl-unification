;;;; -*- Mode: Lisp -*-

;;;; cl-unification-test.asd

(in-package #:asdf)

;; Tests implemented using the ptester framework are run at *load* time, so
;; we tell ASDF that loading a file containing ptester code is never done.
;; This causes ASDF to run all the tests whenever ASDF:LOAD-SYSTEM or
;; ASDF:TEST-SYSTEM is called with argument CL-UNIFICATION-TEST.

(defclass ptester-source-file (cl-source-file)
  ()
  (:documentation "A Common Lisp source file containing ptester code."))

(defmethod operation-done-p ((operation load-op) (component ptester-source-file))
  nil)


(in-package #:common-lisp-user)

(defpackage #:cl-unification-test-system
  (:use #:common-lisp #:asdf))

(in-package #:cl-unification-test-system)

(defsystem #:cl-unification-test
  :depends-on (:cl-unification :ptester)
  :components
  ((:module "test"
    :components
    ((:ptester-source-file "unification-tests")))))

;;;; end of file -- cl-unification-test.asd
