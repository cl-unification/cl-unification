;;;; -*- Mode: Lisp -*-

;;;; cl-unification.system --
;;;; MK:DEFSYSTEM system file.

;;;; See file COPYING for copyright and licensing information.

(mk:defsystem "CL-UNIFICATION"
  :author "Marco Antoniotti"

  :licence "BSD"

  :documentation "The CL-UNIFICATION system.

The system contains the definitions for the 'unification' machinery."
  
  :source-extension "lisp"
  :components ((:file "cl-unification-pkg")
               (:file "variables"
		      :depends-on ("cl-unification-pkg"))
               (:file "substitutions"
		      :depends-on ("variables"))
               (:file "lambda-list-parsing"
		      :depends-on ("cl-unification-pkg"))
               (:file "templates-hierarchy"
		      :depends-on ("variables"))
               (:file "unifier"
		      :depends-on ("templates-hierarchy"
				   "substitutions"
				   "lambda-list-parsing"))
               (:file "match-block"
		      :depends-on ("unifier"))
               (:file "apply-substitution"
		      :depends-on ("unifier"))
               (:module "lib-dependent"
			:depends-on ("unifier")
			:components ((:subsystem "cl-ppcre-template"
						 :non-required-p t
						 )
				     ))
               ))

;;; end of file -- cl-unification.system --
