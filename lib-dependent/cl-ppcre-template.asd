;;;; -*- Mode: Lisp -*-

;;;; cl-ppcre-template.asd --
;;;; ASDF system file.

(asdf:defsystem cl-ppcre-template
  :author "Marco Antoniotti"

  :license "BSD"

  :description
  "A system used to conditionally load the CL-PPCRE Template.

This system is not required and it is handled only if CL-PPCRE is
available.  If it is, then the library provides the
REGULAR-EXPRESSION-TEMPLATE."

  :components ((:file "cl-ppcre-template"))
  :depends-on (cl-ppcre cl-unification))

;;;; end of file -- cl-ppcre-template.asd --
