;;;; -*- Mode: Lisp -*-

;;;; templates-hierarchy.lisp --

;;;; See file COPYING for copyright licensing information.

(in-package "CL.EXT.DACF.UNIFICATION") ; DACF = Data And Control Flow.

;;; Templates.
;;; Essentially we extend the type specifier language.
;;; The interesting thing is that we need to specify how a "match"
;;; between a template and a CL object is performed.

;;; A template is one of the following
;;;
;;;    <template> ::= <logical variable>
;;;               |   <structure template>
;;;               |   <instance template>
;;;               |   <destructuring template>
;;;               |   <vector template>
;;;               |   <sequence template>
;;;               |   <array template>
;;;               |   <type template>
;;;               |   <lisp object>
;;;

;;; Destructuring Template Lambda List (as per standard CL terminology)
;;;
;;;     <destructuring template lambda list> ::= <a "destructuring LL" with <template> in lieu of <var>>

;;; Templates for Structures and Instances
;;;
;;;	<structure template> ::= (<class designator> <structure-slot-spec>*)
;;;
;;;     <instance template>  ::= (<class designator> <slot-spec>*)
;;;
;;; where
;;;
;;;     <structure-slot-spec> ::= <reader-name> <template>
;;;     <instance-slot-spec>  ::= <accessor-name> <template>
;;;                           |   (slot-value <slot-name>) <template>

;;; Template for Sequences
;;;
;;;     <sequence template>    ::= (<container type> . <destructuring template lambda list>)
;;;                            |   (subseq <from> <to> . <destructuring template lambda list>)
;;;     <container type>       ::= list | cons | vector | array

;;; Templates for Vectors and Arrays.
;;;
;;;     <vector template> ::= (vector . <destructuring template lambda list>)
;;;
;;;     <array template>  ::= (array <shape template>)
;;;                       |   (<array CL type specifier> <shape template>)
;;;                       |   (array (['*' | <element type>] [<dimension spec>]]) <shape template>)
;;;                       |   (aref <index template> <template>)
;;;     <shape template>  ::= <destructuring template lambda list>
;;;                       |   <sequence template>
;;;                       |   (<shape template>)

;;; Templates for LIST and CONS
;;;     <list template>   ::= (list . <destructuring template lambda list>)
;;;     <cons template>   ::= (cons <template> <template>)

;;; A regular list or cons acts as a list (hence as a sequence) template, or a cons template.

(define-condition unification-template-error (simple-error)
  ())


;;; Templates are introduced by the reader macro #T(...)

;;; template
;;; ========

(defclass template ()
  ((spec :accessor template-spec :type (or symbol cons) :initarg :spec))
  (:default-initargs :spec nil)
  (:documentation "The TEMPLATE Class.

The root of the hierarchy of template objects.")
   )

(defgeneric template-p (x)
  (:documentation "Returns T if the argument X is a TEMPLATE.

Arguments and Values:

X : any object
result : a BOOLEAN
")
  (:method ((x template)) t)
  (:method ((x t)) nil))


;;; type-template
;;; =============

(defclass type-template (template) ())

(defgeneric type-template-p (x)
  (:method ((x type-template)) t)
  (:method ((x t)) nil))


(defgeneric type-template-type-spec (x)
  (:method ((x type-template))
   (let ((spec (template-spec x)))
     (if spec
         (first spec)
         'null))))


;;; nil-template
;;; ============

(defclass nil-template (type-template) ()) ; This is the point where we break the type hierarchy.

(defgeneric nil-template-p (x)
  (:method ((x nil-template)) t)
  (:method ((x t)) nil))


;;; expression-template
;;; ===================

(defclass expression-template (template)
  ()
  (:documentation "The EXPRESSION-TEMPLATE Class.

This class is the root of the 'expression template' sub-hierarchy."))

(defgeneric expression-template-p (x)
  (:method ((x expression-template)) t)
  (:method ((x t)) nil))


(defmethod print-object ((template template) stream)
  (format stream "#T~S" (template-spec template)))


;;; sequence-template
;;; =================

(defclass sequence-template (type-template) ())

(defgeneric sequence-template-p (x)
  (:method ((x sequence-template)) t)
  (:method ((x t)) nil))


;;; list-template
;;; =============

(defclass list-template (sequence-template) ())

(defgeneric list-template-p (x)
  (:method ((x list-template)) t)
  (:method ((x t)) nil))


;;; 20121204 experimental

;;; plist-template
;;; ==============

(defclass plist-template (list-template) ())

(defgeneric plist-template-p (x)
  (:method ((x plist-template)) t)
  (:method ((x t)) nil))


;;; lambda-template
;;; ===============

(defclass lambda-template (list-template expression-template) ())

(defgeneric lambda-template-p (x)
  (:method ((x lambda-template)) t)
  (:method ((x t)) nil))


;;; array-template
;;; ==============

(defclass array-template (type-template)
  ()
  (:documentation "The ARRAY-TEMPLATE Class.

The instances of this class are objects that can be used to unify
against arrays.

The syntax of this particular template is the following:
<pre>
  #T(array &lt;shape-template&gt;)
</pre>

<pre>
  #T(&lt;CL array type specifier&gt; &lt;shape-template&gt;)
</pre>

<pre>
  #T(array ([* | &lt;CL type specifier&gt;] [&lt;dimension spec&gt;]) &lt;shape-template&gt;)
</pre>

Where &lt;shape-template&gt; can be:

<pre>
  &lt;shape-template&gt; ::= &lt;sequence-template&gt;
                   |   &lt;destructuring template lambda list&gt;
                   |   (&lt;shape-template&gt;)
</pre>

The ARRAY-TEMPLATE syntax denotes an ARRAY object.  An ARRAY-TEMPLATE
must be unified against an ARRAY object. The elements of the array
must be unified against the &lt;shape-template&gt;.  Each row of the
array is unified recursively against each element of the
&lt;shape-template&gt;.


Examples:

cl-prompt> (setf e (unify #(0 1 42 3 4 5) #T(array (0 1 ?x 3 4 5))))
#<ENVIRONMENT xxx>
 
cl-prompt> (find-variable-value '?x e)
42
 
cl-prompt> (setq e (unify #(0 1 42 3 4 5)  #T(array (0 1 \"FOO\" 3 4 5))))
==> Error: UNIFICATION-FAILURE
 
cl-prompt> (setq e (unify #2A((0 1 42) (3 4 5)) #T(array ((0 1 ?x) (3 4 5)))))
#<ENVIRONMENT xxx>
 
cl-prompt> (find-variable-value '?x e)
42
 
cl-prompt> (setq e (unify #2A((\"foo\" \"bar\" 42) (3 4 5)) #T(array ((_ _ ?x) (3 4 5)))))
#<ENVIRONMENT xxx>
 
cl-prompt> (find-variable-value '?x e)
42
 
cl-prompt> (setq e (unify #2A((\"foo\" \"bar\" 42) (3 4 5)) #T(array (#T(vector _ &rest ?x) (3 4 5)))))
#<ENVIRONMENT xxx>
 
cl-prompt> (find-variable-value '?x e)
#(\"bar\" 42)


Exceptional Situations:

Unifying an ARRAY-TEMPLATE against a non-ARRAY object results in an
UNIFICATION-FAILURE error being signaled.


Notes:

<h3>Syntax Note</h3>

The ARRAY-TEMPLATE syntax tries to be easy to use, at the cost of
being overloaded.  There is no actual need to have the separate
forms <code>(<b>array</b> (fixnum) ...)</code> and
<code>((<b>array</b> fixnum) ...)</code>.

In a future release they may be conflated.  For the time being
they are kept separate as it is unclear which would be better to
provide.

<h3>Elements Unification</h3>

It would be nice to have the possibility to unify against
<em>slices</em> of the array.  Alas, this seems non trivial to get
right, and it is left as a future extension of the semantics of
<i>&lt;shape-template&gt;</i>.

<h3>ARRAY Structural Properties</h3>

For the time being, there is no way to \"unify\" against structural
properties of vectors like fill pointers and displacements.
")
  )

(defgeneric array-template-p (x)
  (:method ((x array-template)) t)
  (:method ((x t)) nil))


;;; vector-template
;;; ===============

(defclass vector-template (sequence-template array-template) ())

(defgeneric vector-template-p (x)
  (:method ((x vector-template)) t)
  (:method ((x t)) nil))


;;; string-template
;;; ===============

(defclass string-template (vector-template) ())

(defgeneric string-template-p (x)
  (:method ((x string-template)) t)
  (:method ((x t)) nil))


;;; symbol-template
;;; ===============

(defclass symbol-template (type-template) ())

(defgeneric symbol-template-p (x)
  (:method ((x symbol-template)) t)
  (:method ((x t)) nil))


;;; number-template
;;; ===============

(defclass number-template (type-template)
  ())

(defgeneric number-template-p (x)
  (:method ((x number-template)) t)
  (:method ((x t)) nil))


;;; structure-object-template
;;; =========================

(defclass structure-object-template (type-template) ())

(defgeneric structure-object-template-p (x)
  (:method ((x structure-object-template)) t)
  (:method ((x t)) nil))


;;; standard-object-template
;;; ========================

(defclass standard-object-template (type-template) ())

(defgeneric standard-object-template-p (x)
  (:method ((x standard-object-template)) t)
  (:method ((x t)) nil))


;;; Expression Templates
;;; ====================

;;; subseq-template
;;; ---------------

(defclass subseq-template (expression-template) ())

(defgeneric subseq-template-p (x)
  (:method ((x subseq-template)) t)
  (:method ((x t)) nil))


;;; element-template
;;; ----------------

(defclass element-template (expression-template)
  ()
  (:documentation "The ELEMENT-TEMPLATE Class.

The instances of this class are those object that are used to unify
against a particular element of an ARRAY or a SEQUENCE."))

(defgeneric element-template-p (x)
  (:method ((x element-template)) t)
  (:method ((x t)) nil))


;;; elt-template
;;; ------------

(defclass elt-template (element-template) ())

(defgeneric elt-template-p (x)
  (:method ((x elt-template)) t)
  (:method ((x t)) nil))


;;; aref-template
;;; -------------

(defclass aref-template (element-template)
  ()
  (:documentation "The AREF-TEMPLATE Class.

The instances of this class are those object that are used to unify
against a particular element of an ARRAY.

The syntax of these templates is the following:

<pre>
  #T(<b>aref</b> (<i>index1</i> ... <i>indexN</i>) <i>item</i>)
</pre>

<pre>
  #T(<b>aref</b> <i>index</i> <i>item</i>)
</pre>

The AREF-TEMPLATE syntax denotes the <i>item</i> at <i>index1</i> ...
<i>indexN</i> of an ARRAY.  An AREF-TEMPLATE must be unified against a
ARRAY object.  <i>item</i> is <em>unified</em> against the element
extracted from the sequence object at <i>index1</i> ... <i>indexN</i>
by the standard function AREF.

The second form is a shorthand.

<pre>
  #T(<b>aref</b> <i>index</i> <i>item</i>) <==> #T(<b>aref</b> (<i>index</i>) <i>item</i>)
</pre>


Examples:

cl-prompt> (setf e (unify #(0 1 42 3 4 5) #T(aref 2 ?x)))
#<ENVIRONMENT xxx>

cl-prompt> (find-variable-value '?x e)
42

cl-prompt> (setq e (unify #(0 1 42 3 4 5) #T(aref 42 ?x)))
==> Error: index 42 out of bounds.

cl-prompt> (setq e (unify 42 #T(aref 42 ?x)))
==> Error: UNIFICATION-FAILURE

cl-prompt> (setq e (unify \"This is a string!\" #T(aref 4 ?x)))
#<ENVIRONMENT xxx>

cl-prompt> (find-variable-value '?x e)
#\\Space  

cl-prompt> (setq e (unify #2A((1 0) (foo bar)) #T(aref (1 1) ?x)))
#<ENVIRONMENT xxx>

cl-prompt> (find-variable-value '?x e)
BAR

cl-prompt> (setq e (unify #2A((1 0) (foo bar)) #T(aref (1 1) baz)))
==> Error: UNIFICATION-FAILURE

cl-prompt> (setq e (unify #2A((1 0) (foo ?x)) #T(aref (1 1) baz)))
#<ENVIRONMENT xxx>

cl-prompt> (find-variable-value '?x e)
BAZ


Exceptional Situations:

Unifying an AREF-TEMPLATE against a non-ARRAY object results in an
UNIFICATION-FAILURE error being signaled.")
  )

(defgeneric aref-template-p (x)
  (:method ((x aref-template)) t)
  (:method ((x t)) nil))


;;; nth-template
;;; ------------

(defclass nth-template (element-template) ())

(defgeneric nth-template-p (x)
  (:method ((x nth-template)) t)
  (:method ((x t)) nil))


;;; nthcdr-template
;;; ---------------

(defclass nthcdr-template (element-template) ())

(defgeneric nthcdr-template-p (x)
  (:method ((x nthcdr-template)) t)
  (:method ((x t)) nil))


;;; Template instantiation
;;; ======================

(defgeneric make-template (kind spec)
  (:documentation "The 'template' factory.

Creates a TEMPLATE instance based on the values of KIND and SPEC.

Arguments and Values:

KIND : a T
SPEC : a T
result : a TEMPLATE

Exceptional Situations:

May signal an error if a particular combination of KIND and SPEC is either
not consistent or handled."))


;;; #T reader macro
;;; ---------------
;;;
;;; Setting up the reader macro.

;;; 20080711 MA:
;;; Reverted to the old version with MAKE-LOAD-FORM added.  Template
;;; objects are created at read-time.

(defun |sharp-T-reader| (stream subchar arg)
  (declare (ignore subchar arg))
  (let ((spec (read stream t nil t)))
    (typecase spec
      (null (make-template nil spec))
      (cons (make-template (first spec) spec))
      (t    (make-template spec spec)))))


(defmethod make-load-form ((x template) &optional env)
  (make-load-form-saving-slots x :environment env))


#|
;;; Version with more 'macro-like' behavior. The previous version
;;; created an object at read-time, which may cause problems with
;;; MAKE-LOAD-FORMs, constant-ness etc etc.
;;;
;;; 20080713 MA
;;; Removed because it was not working well with nested templates.
;;; Reverted to the original one plus MAKE-LOAD-FORM.

(defun |sharp-T-reader| (stream subchar arg)
  (declare (ignore subchar arg))
  (let ((spec (read stream t nil t)))
    (typecase spec
      (null `(make-template nil ',spec))
      (cons `(make-template ',(first spec) ',spec))
      (t    `(make-template ',spec ',spec)))
    ))
|#

(eval-when (:load-toplevel :execute)
  (set-dispatch-macro-character #\# #\T '|sharp-T-reader|))


#|  Useless with the read time templates and MAKE-LOAD-FORM.

(defun rewrite-template-spec (spec)
  "Rewrites a template specification.
The rewriting simply makes sure that sub-templates are created as needed.
The result is either the SPEC itself or an appropriate call to LIST."

  (typecase spec
    (atom `',spec)
    (cons (destructuring-bind (head &rest tail)
              spec
            (case head
              (quote spec)
              (make-template `(make-template ,(first tail)
                                             ,(rewrite-template-spec (second (second tail)))))
              (t `(list ',head ,@(mapcar #'rewrite-template-spec tail)))
              )))
    (t `',spec)))

|#


(defmethod make-template ((kind null) (spec symbol))
  (assert (null spec) (spec)
	  "MAKE-TEMPLATE called erroneously with ~S and ~S."
	  kind spec)
  (make-instance 'nil-template :spec spec))

(defmethod make-template ((kind symbol) (spec symbol))
  (make-instance 'symbol-template :spec spec))

(defmethod make-template ((kind (eql 'symbol)) (spec cons))
  (make-instance 'symbol-template :spec spec))

(defmethod make-template ((kind symbol) (spec cons))
  (cond ((subtypep kind 'number)
         (make-instance 'number-template :spec spec))
        ((subtypep kind 'structure-object)
         (make-instance 'structure-object-template :spec spec))
        ((subtypep kind 'standard-object)
         (make-instance 'standard-object-template :spec spec))
        (t
         (error 'unification-template-error
                :format-control "Unknown template specifier ~S."
                :format-arguments (list kind)))
        ))

(defmethod make-template ((kind cons) (spec cons))
  (cond ((subtypep kind 'number)
         (make-instance 'number-template :spec spec))
        ((subtypep kind 'string)
         (make-instance 'string-template :spec spec))
        ((subtypep kind 'vector)
         (make-instance 'vector-template :spec spec))
        ((subtypep kind 'array)
         (make-instance 'array-template :spec spec))
        (t
         (error 'unification-template-error
                :format-control "Unknown template specifier ~S."
                :format-arguments (list kind)))
        ))

(defmethod make-template ((kind number) (spec number))
  (assert (= kind spec))
  (make-instance 'number-template :spec spec))

(defmethod make-template ((kind (eql 'sequence)) (spec cons))
  (make-instance 'sequence-template :spec spec))

(defmethod make-template ((kind (eql 'list)) (spec cons))
  (make-instance 'list-template :spec spec))

(defmethod make-template ((kind (eql 'plist)) (spec cons))
  (make-instance 'plist-template :spec spec))

(defmethod make-template ((kind (eql 'lambda)) (spec cons))
  (make-instance 'lambda-template :spec spec))

(defmethod make-template ((kind (eql 'vector)) (spec cons))
  (make-instance 'vector-template :spec spec))

(defmethod make-template ((kind (eql 'string)) (spec cons))
  (make-instance 'string-template :spec spec))

(defmethod make-template ((kind (eql 'array)) (spec cons))
  (make-instance 'array-template :spec spec))


(defmethod make-template ((kind (eql 'subseq)) (spec cons))
  (make-instance 'subseq-template :spec spec))

(defmethod make-template ((kind (eql 'elt)) (spec cons))
  (make-instance 'elt-template :spec spec))

(defmethod make-template ((kind (eql 'aref)) (spec cons))
  (make-instance 'aref-template :spec spec))

(defmethod make-template ((kind (eql 'nth)) (spec cons))
  (make-instance 'nth-template :spec spec))

(defmethod make-template ((kind (eql 'nthcdr)) (spec cons))
  (make-instance 'nthcdr-template :spec spec))



;;;;===========================================================================
;;;; Implementation.

;;; Symbol Templates.
;;; Specification is
;;;
;;;    (symbol <symbol>)

(defun symbol-template-symbol (x)
  (declare (type symbol-template x))
  (assert (symbol-template-p x) (x) "Non symbol template ~S." x)
  (let ((spec (template-spec x)))
    (cond ((symbolp spec) spec)
          ((consp spec) (second spec)))))


;;; Number template
;;; Specification is
;;;
;;;	(<number type> <number>)
;;; or
;;;
;;;	<number>
;;;
;;; Initialize the instance by checking a few constraints.

(defparameter *known-number-template-constants*
  '(pi))


(defmethod initialize-instance :before ((nt number-template) &key spec &allow-other-keys)
  (etypecase spec
    (symbol (assert (or (member spec *known-number-template-constants*
                                :test #'eq)
                        (variablep spec)) ()
              "~A is a symbol but not an unification variable." spec))
    (number spec)
    (cons (destructuring-bind (number-type-spec n)
	      spec
	    (assert (subtypep number-type-spec 'number))
	    (etypecase n
              (number
               (unless (typep n number-type-spec)
                 (error 'unification-template-error
                        :format-control "Number type clash in NUMBER-TEMPLATE: ~S and ~S."
                        :format-arguments spec)))
              (symbol (assert (or (member spec *known-number-template-constants*
                                          :test #'eq)
                                  (variablep n)) ()
                        "~A is a symbol but not an unification variable."
                        n))
	    )))))


(defmethod initialize-instance :after ((nt number-template) &key spec &allow-other-keys)
  (when (consp spec)
    (destructuring-bind (number-type-spec n)
        spec
      ;; Let's constant fold a few known ones.
      (case n
        (pi  (if (not (subtypep number-type-spec (type-of pi)))
                 (error 'unification-template-error
                        :format-control "Number type clash in NUMBER-TEMPLATE: ~S and ~S."
                        :format-arguments spec)
                 (setf (template-spec nt)
                       `(,number-type-spec ,pi))))))
    ))


(defun number-template-number (x)
  (declare (type number-template x))
  (assert (number-template-p x) (x) "Non number template ~S." x)
  (let ((spec (template-spec x)))
    (etypecase spec
      (symbol spec)
      (number spec)
      (cons (second spec)))))


(defun number-template-numeric-type (x)
  (declare (type number-template x))
  (let ((n (number-template-number x)))
    (if (numberp n)
        (type-of n)
        (first (template-spec x)))))

(defun number-template-numeric-class (x)
  (declare (type number-template x))
  (let ((n (number-template-number x)))
    (if (numberp n)
        (class-of n)
        (find-class (first (template-spec x))))))


;;; Sequence Templates.
;;; Specification is
;;;
;;;	(<sequence subtype> . <destructuring template lambda list>)
;;; or
;;;     (subseq <from> <to> . <destructuring template lambda list>)

(defun sequence-template-lambda-list (x)
  (declare (type sequence-template x))
  (assert (sequence-template-p x) (x) "Non sequence template ~S." x)
  (rest (template-spec x)))


;;; Vector Templates.
;;; Specification is
;;;
;;;	(<vector type specifier> . <destructuring template lambda list>)

(defun vector-template-element-type (x)
  (declare (type vector-template x))
  (assert (vector-template-p x) (x) "Non vector template ~S." x)
  (let ((spec (type-template-type-spec x)))
    (if (consp spec)
        (destructuring-bind (vector-kwd &optional (element-type '*) size)
            spec
          (declare (ignore vector-kwd size))
          element-type)
        '*)))


(defun vector-template-size (x)
  (declare (type vector-template x))
  (assert (vector-template-p x) (x) "Non vector template ~S." x)
  (let ((spec (type-template-type-spec x)))
    (if (consp spec)
        (destructuring-bind (vector-kwd &optional element-type (size '*))
            spec
          (declare (ignore vector-kwd element-type))
          size)
        '*)))


;;; Array Templates.
;;; Specification is
;;;
;;;	(array (['*' | <element type>] [<dimension spec>])  <shape template>)
;;; or
;;;	(<array type specifier> <shape template>)

(defun array-template-shape-template (x)
  (declare (type array-template x))
  (assert (array-template-p x) (x) "Non array template ~S." x)
  (let ((t-spec (template-spec x)))
    (if (= 2 (list-length t-spec))
        (second t-spec)
        (third t-spec))))


(defun array-template-element-type (x)
  (declare (type array-template x))
  (assert (array-template-p x) (x) "Non array template ~S." x)
  (let ((type-spec (type-template-type-spec x)))
    (if (consp type-spec)
        (destructuring-bind (array-kwd &optional (element-type '*) dimension-spec)
            type-spec
          (declare (ignore array-kwd dimension-spec))
          element-type)
        '*)))


(defun array-template-dimensions (x)
  (declare (type array-template x))
  (assert (array-template-p x) (x) "Non array template ~S." x)
  (let ((type-spec (type-template-type-spec x)))
    (if (consp type-spec)
        (destructuring-bind (array-kwd &optional element-type (dimension-spec '*))
            type-spec
          (declare (ignore array-kwd element-type))
          dimension-spec)
        '*)))


;;; Structure and Standard Object Templates.

(defun structure-object-template-class (x)
  (and (structure-object-template-p x)
       (first (template-spec x))))

(defun structure-object-template-slots (x)
  (and (structure-object-template-p x)
       (rest (template-spec x))))


(defun standard-object-template-class (x)
  (and (standard-object-template-p x)
       (first (template-spec x))))

(defun standard-object-template-slots (x)
  (and (standard-object-template-p x)
       (rest (template-spec x))))


;;;---------------------------------------------------------------------------
;;; Expression Templates.

;;; AREF Templates.

(defun aref-template-indexes (x)
  (declare (type aref-template x))
  (assert (aref-template-p x) (x) "Non aref template ~S." x)
  (let ((spec (template-spec x)))
    (second spec)))


(defun aref-template-element (x)
  (declare (type aref-template x))
  (assert (aref-template-p x) (x) "Non array template ~S." x)
  (let ((spec (template-spec x)))
    (third spec)))


;;;===========================================================================
;;; Template variables.
;;; Let's walk down a template.
;;; Note that there is an asymmetry here: I admit some containers to have
;;; variables inside, but I do not search instances of a class for variables.
;;; This is an asymmetry that would be way too hard to fix without more
;;; introspective power (which is available in the MOP, but not
;;; standard.)  Let's not despair though: CLAST is on its way.


(defgeneric collect-template-vars (template))

(defmethod collect-template-vars ((template template))
  (let ((spec (template-spec template)))
    (nconc (collect-template-vars (car spec))
           (collect-template-vars (cdr spec)))))

(defmethod collect-template-vars ((template symbol-template))
  (let ((template (symbol-template-symbol template)))
    (when (and (variablep template) (not (variable-any-p template)))
      (list template))))


(defmethod collect-template-vars ((template number-template))
  (let ((template (number-template-number template)))
    (etypecase template
      (number ())
      (symbol (cond ((and (variablep template) (not (variable-any-p template)))
                     (list template))
                    ((and (boundp template)
                          (numberp (symbol-value template)))
                     ;; This handles cases like #T(number pi)
                     ;; It may be too broad, but for the time being it seems ok.
                     nil)
                    (t
                     (error "Invalid number template ~S." template)))))))


(defmethod collect-template-vars ((template symbol))
  (when (and (variablep template) (not (variable-any-p template)))
    (list template)))

(defmethod collect-template-vars ((template null))
  ())

(defmethod collect-template-vars ((template cons))
  (nconc (collect-template-vars (car template))
         (collect-template-vars (cdr template))))

(defmethod collect-template-vars ((template string))
  ())


(defmethod collect-template-vars ((template vector))
  (loop for e across template
        nconc (collect-template-vars e)))


(defmethod collect-template-vars ((template array))
  (loop for i below (array-total-size template)
        nconc (collect-template-vars (row-major-aref template i))))


(defmethod collect-template-vars ((template t))
  ())

;;; end of file -- templates.lisp --
